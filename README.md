
# APORT - Advance Page Object Resolver Toolkit

 Several classes for declarative component-oriented approach to writing tests
 using **Selenium 2 WebDriver, Java**.

 Released as FOSS under **MIT license**.

 Hosted on **Bitbucket**:
 [https://bitbucket.org/rostislav_matl/aport](https://bitbucket.org/rostislav_matl/aport)

## Artifacts

### Releases

* 2017-01-10, **version 0.3**, first release, API considered stable
* 2017-06-27, **version 1.0**, upgrade to Selenium 3.x

### Snapshots

 Snapshots are quite stable. i.e. bugs are fixed at top priority.
 API changes are mostly backward-compatible but API still can change.

 [https://oss.sonatype.org/content/repositories/snapshots/org/bithill/selenium/aport/](https://oss.sonatype.org/content/repositories/snapshots/org/bithill/selenium/aport/)

 Add this to your maven setting if you want to use snapshot version in your project:

> <?xml version="1.0" encoding="UTF-8"?>
>
> <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
>           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
>           xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
>
>     <activeProfiles>
>         <activeProfile>ossrh</activeProfile>
>     </activeProfiles>
>
>     <profiles>
>         <profile>
>             <id>ossrh</id>
>             <repositories>
>                 <repository>
>                     <id>ossrh-snapshot</id>
>                     <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
>                     <snapshots>
>                         <enabled>true</enabled>
>                     </snapshots>
>                 </repository>
>             </repositories>
>         </profile>
>     </profiles>
>  </settings>
>

## For Developers

Contributors are welcome.

### Code Style

 Respect existing code style.

 In case you are using Intellij IDEA, there is code style for recent version in aport.xml file.

### OSSRH, Related Info -- Issues etc.

 https://issues.sonatype.org/browse/OSSRH-17892