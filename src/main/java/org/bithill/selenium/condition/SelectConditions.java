package org.bithill.selenium.condition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;

/** Collection of {@link ExpectedCondition}s for {@link Select}. */
public class SelectConditions
{
    /**
     * Condition met when a &lt;select&gt; has at least given count of &lt;option&gt;s.
     *
     * @param locator locator of the select
     * @param minValuesCount minimal number of options required
     * @return the matching select
     */
    public static ExpectedCondition<Select> hasAtLeastValues(final By locator, final Integer minValuesCount)
    {
        return new ExpectedCondition<Select>()
        {
            private int currentValueCount = 0;

            @Override
            public Select apply(WebDriver driver)
            {
                WebElement element = driver.findElement(locator);
                if (element.isDisplayed())
                {
                    Select select = new Select(element);
                    currentValueCount = select.getOptions().size();
                    if (currentValueCount >= minValuesCount) { return select; }
                }
                return null;
            }

            public String toString()
            {
                return String.format("Value count should be >= %d. Current count: %d",
                                     minValuesCount, currentValueCount);
            }
        };
    }
}
