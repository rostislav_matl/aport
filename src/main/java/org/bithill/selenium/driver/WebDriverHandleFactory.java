package org.bithill.selenium.driver;

import java.util.Map;

/**
 * Factory interface for {@link WebDriverHandle WebDriverHandles}.
 */
public interface WebDriverHandleFactory<T extends WebDriverHandle>
{
    /** Creates WebDriverHandle matching given criteria.
     *
     * @param criteria criteria affecting the to-be-created WebDriverHandle.
     *
     *                 <div>
     *                 Should be passed to the WebDriverHandle's constructor
     *                 as optional 'info' argument so the WebDriverHandle instance
     *                 keeps information about from what settings it was created.
     *                 </div>
     *
     * @return WebDriverHandle matching given criteria
     *
     * @throws DriverInitializationException when the creation and initialization of the driver fails
     */
    T createDriverHandle(Map<String,String> criteria);
}
