package org.bithill.selenium.driver;

import java.util.Map;

/**
 * Signals that an driver exception of some sort has occurred.
 *
 * This is supposed to be root class for all of exceptions produced by
 * {@link WebDriverHandleFactory#createDriverHandle(Map) driver factory's create method}.
 *
 * If this Exception is thrown in a reaction to another exception, the original exception MUST be always included,
 * i.e. a constructor with cause has to be used.
 */
public class DriverInitializationException extends RuntimeException {

    static final long serialVersionUID = 1L;

    public DriverInitializationException() { super(); }

    public DriverInitializationException(String message) { super(message); }

    public DriverInitializationException(Throwable cause) { super(cause); }

    public DriverInitializationException(String message, Throwable cause) { super(message, cause); }
}
