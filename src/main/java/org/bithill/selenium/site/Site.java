package org.bithill.selenium.site;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.bithill.selenium.resolving.Page;

/**
 * Annotation that associated a {@link Page} with a {@link SiteConfig}.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Site
{
    Class<? extends SiteConfig> config();
}
