package org.bithill.selenium.site;

/**
 * Minimal 'functional' {@link SiteConfig}.
 * You can use it in case you do want to implement your own for some reasons.
 */
public class MinimalSiteConfig extends SiteConfig
{
    @Override
    public String getUrl()
    {
        return "";
    }
}
