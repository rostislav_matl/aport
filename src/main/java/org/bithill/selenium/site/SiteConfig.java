package org.bithill.selenium.site;

import java.io.File;

import org.bithill.selenium.resolving.Page;

/**
 * Basic setup of a web site (web application).
 * Contains and provides an information shared by all the pages of a site.
 */
public abstract class SiteConfig
{
    /**
     * Site address.
     *
     * @return URL of the site's entry page
     */
    public abstract String getUrl();

    /**
     * Screenshot directory.
     *
     * @return path to a site-wide directory where the {@link Page} classes should store screenshots
     */
    public String getScreenshotDir()
    {
        return System.getProperty("user.dir") + File.separatorChar + "screenshots";
    }
}
