package org.bithill.selenium.resolving;

/** Thrown when the locator used in {@link ResolveBy} annotation is not recognized as valid locator.
 * Does not apply to well-formed locator matching no node.
 */
public class InvalidLocatorException extends RuntimeException
{
  public InvalidLocatorException ()
  {
    super();
  }

  public InvalidLocatorException (String message)
  {
    super(message);
  }

  public InvalidLocatorException (Throwable cause)
  {
    super(cause);
  }

  public InvalidLocatorException (String message, Throwable cause)
  {
    super(message, cause);
  }
}
