package org.bithill.selenium.resolving;

/** Thrown when the locator used in {@link ResolveBy} has invalid format,
 * i.e. is missing delimiter.
 */
public class InvalidLocatorFormatException extends InvalidLocatorException
{
  public InvalidLocatorFormatException ()
  {
    super();
  }

  public InvalidLocatorFormatException (String message)
  {
    super(message);
  }

  public InvalidLocatorFormatException (Throwable cause)
  {
    super(cause);
  }

  public InvalidLocatorFormatException (String message, Throwable cause)
  {
    super(message, cause);
  }
}
