package org.bithill.selenium.resolving;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.bithill.selenium.driver.WebDriverHandle;
import org.openqa.selenium.WebElement;

/**
 * Annotation for the attaching the {@link org.openqa.selenium.By WebDriver selector}
 * to a {@link org.openqa.selenium.WebElement WebElement}.
 *
 * This exception should not appear in test code as it is always caused by typo.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ResolveBy
{
    String DEFAULT_GROUP = "default";

    /** @return String representation of the {@link org.openqa.selenium.By locator} */
    String value();

    /**
     * Group of to which {@link WebElement} or {@link Resolvable} belongs to.
     * Allows resolve only given set of resolvable elements by {@link Page#resolve()}.
     *
     * @return list of groups the annotated element belongs to
     */
    String[] groups() default {DEFAULT_GROUP};

    /** If set to true, WebElement fields are resolved relatively to the Resolvable.
     * @return true if it is container, false otherwise
     */
    boolean isContainer() default false;

    /**
     * Name of the wait to use during resolving of an annotated element.
     *
     * @return a name of wait, i.e. key in {@link WebDriverHandle#waits}
     */
    String explicitWait() default WebDriverHandle.DEFAULT_WAIT;
}
