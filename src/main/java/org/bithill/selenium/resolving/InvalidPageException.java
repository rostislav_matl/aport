package org.bithill.selenium.resolving;

/**
 * Thrown when an the expected page is not loaded, regardless if a wrong page is
 * or no page is loaded at all.
 */
public class InvalidPageException extends RuntimeException
{
  public InvalidPageException ()
  {
    super();
  }

  public InvalidPageException (String message)
  {
    super(message);
  }

  public InvalidPageException (Throwable cause)
  {
    super(cause);
  }

  public InvalidPageException (String message, Throwable cause)
  {
    super(message, cause);
  }
}
