package org.bithill.selenium.resolving;

/**
 * Thrown when locator in {@link ResolveBy} annotation has expected format but
 * its type is not one of recognized values: "id", "xpath", "css", "name", "tag", or "link" .
 */
public class UnknownLocatorTypeException extends InvalidLocatorException
{
  public UnknownLocatorTypeException ()
  {
    super();
  }

  public UnknownLocatorTypeException (String message)
  {
    super(message);
  }

  public UnknownLocatorTypeException (Throwable cause)
  {
    super(cause);
  }

  public UnknownLocatorTypeException (String message, Throwable cause)
  {
    super(message, cause);
  }
}
