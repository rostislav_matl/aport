package org.bithill.selenium.resolving;

import java.util.ArrayList;
import java.util.Collection;

/** A List with additional context info used during resolve. */
public class ResolvableList<T extends Resolvable> extends ArrayList<T>
{
    private boolean isContainer = false;
    public boolean isContainer() { return isContainer; }
    public void setIsContainer(boolean isContainer) { this.isContainer = isContainer; }

    public ResolvableList(int initialCapacity) { super(initialCapacity); }
    public ResolvableList() { super(); }
    public ResolvableList(Collection<? extends T> collection) { super(collection); }
}