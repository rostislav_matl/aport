package org.bithill.selenium.resolving;

import org.bithill.selenium.resolving.pageobjects.LinksInComponentLinkExtPage;
import org.bithill.selenium.resolving.pageobjects.LinksInComponentLinkPage;
import org.bithill.selenium.resolving.pageobjects.LinksInComponentWithSelfLinkPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

/** Tests resolving of {@link WebElement}s wrapped in a {@link Resolvable} field of a page. */
public class IncludedResolvableResolvingTest extends TestBase
{
    /**
     * {@link Resolvable}s can form a hierarchy of arbitrary depth.
     * The only constraint is that the top member must be a {@link Page}.
     * <p/>
     * All WebElements annotated with {@link @Resolvable} in such hierarchy
     * will be resolved when Page.resolve() is called.
     */
    @Test
    public void resolveLinksInComponent()
    {
        LinksInComponentLinkPage linkPage = new LinksInComponentLinkPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        Assert.assertEquals(linkPage.links.linkById.getText(), "link with id");
        Assert.assertEquals(linkPage.links.linkByName.getText(), "link with name");

        Assert.assertEquals(linkPage.links.linkByXpath.getText(), "link with nothing special");
        Assert.assertEquals(linkPage.links.linkByLinkText.getText(), "link with nothing special");

        // link with id is the first one, therefore it should be resolved by the tag locator
        Assert.assertEquals(linkPage.links.linkByTag.getText(), "link with id");

        Assert.assertEquals(linkPage.links.linkByCss.getText(), "link with CSS class");
    }

    @Test
    public void resolveLinksInComponentInherited()
    {
        LinksInComponentLinkExtPage linkPage = new LinksInComponentLinkExtPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        assertNotNull(linkPage.links2);

        // inherited field or Resolvable type
        assertNotNull(linkPage.links);
        assertNotNull(linkPage.getLinksPriv());
    }

    /**
     * Resolvable can be annotated by ResolveBy.
     */
    @Test
    public void resolveComponentSelf()
    {
        LinksInComponentWithSelfLinkPage linkPage = new LinksInComponentWithSelfLinkPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        assertNotNull(linkPage.links.getSelf(), "self-reference is missing (null)");
    }
}
