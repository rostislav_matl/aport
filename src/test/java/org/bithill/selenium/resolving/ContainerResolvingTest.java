package org.bithill.selenium.resolving;

import org.bithill.selenium.resolving.pageobjects.LinksInContainerPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

/** Tests resolving of {@link WebElement}s inside a container Resolvable. */
public class ContainerResolvingTest extends TestBase
{
    /**
     * Resolvable can be annotated by ResolveBy.
     */
    @Ignore
    public void resolveComponentSelf()
    {
        LinksInContainerPage linkPage = new LinksInContainerPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        assertNotNull(linkPage.links.getSelf(), "self-reference is missing (null)");
    }

    /**
     * Resolvable can serve as container.
     * This test should ignore links with a different parent, although these would be normally resolved,
     * as they precede the requested elements in our page -- see links.html.
     */
    @Test
    public void resolveContainerContent()
    {
        LinksInContainerPage linkPage = new LinksInContainerPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        Assert.assertEquals(linkPage.links.linkByName.getText(), "link with name #2");
        Assert.assertEquals(linkPage.links.linkByCss.getText(), "link with CSS class #2");
    }
}
