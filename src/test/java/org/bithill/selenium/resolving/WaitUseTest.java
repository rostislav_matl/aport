package org.bithill.selenium.resolving;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bithill.selenium.driver.WebDriverHandle;
import org.bithill.selenium.resolving.pageobjects.NonExistentLinkComponentPage;
import org.bithill.selenium.resolving.pageobjects.NonExistentLocatorPage;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.bithill.selenium.driver.WebDriverHandle.DEFAULT_WAIT;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/** Tests of waits' length customization. */
public class WaitUseTest extends TestBase
{
    private final long TIMEOUT_SECONDS = WebDriverHandle.DEFAULT_TIMEOUT_SECONDS + 5;
    private Pattern timeoutMessage = Pattern.compile("(tried for (\\d+) second\\(s\\) with 500 milliseconds interval)");
    FluentWait<WebDriver> originalWait;

    @BeforeClass
    @Override
    public void setup()
    {
        super.setup();
        originalWait = driverHandle.getWaits().get(DEFAULT_WAIT);
        driverHandle.getWaits().put(DEFAULT_WAIT, new WebDriverWait(driverHandle.getDriver(), TIMEOUT_SECONDS));
    }

    @Test
    public void customTimeoutForInlineElements()
    {
        Page page = new NonExistentLocatorPage(driverHandle);
        checkWaitTimeoutSetup(driverHandle.getWaits().get(DEFAULT_WAIT), page);
    }

    @Test
    public void customTimeoutForElementsInComponent()
    {
        Page page = new NonExistentLinkComponentPage(driverHandle);
        checkWaitTimeoutSetup(driverHandle.getWaits().get(DEFAULT_WAIT), page);
    }

    private void checkWaitTimeoutSetup(FluentWait<WebDriver> customWait, Page page)
    {
        boolean timeoutExceptionThrown = false;

        page.setWait(customWait);

        try
        {
            page.resolve();
        }
        catch (TimeoutException ex)
        {
            timeoutExceptionThrown = true;
            String exceptionMessage = ex.getMessage();
            Matcher messageMatcher = timeoutMessage.matcher(exceptionMessage);

            if (messageMatcher.find())
            {
                long secondsWaited = Long.valueOf(messageMatcher.group(2));
                assertEquals(secondsWaited, TIMEOUT_SECONDS, "Wait length was not propagated to included component.");
            }
            else
            {
                Assert.fail("Exception message does not contain information about timeout.");
            }
        }

        assertTrue(timeoutExceptionThrown, "There was no TimeoutException.");
    }

    @AfterClass
    public void resetWait()
    {
        driverHandle.getWaits().put(DEFAULT_WAIT, originalWait);
    }
}
