package org.bithill.selenium.resolving;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.bithill.selenium.TestDriverFactory;
import org.bithill.selenium.driver.WebDriverHandle;
import org.bithill.selenium.driver.WebDriverHandleFactory;
import org.bithill.selenium.resolving.pageobjects.FieldTypesComponent;
import org.bithill.selenium.resolving.pageobjects.TableRowComponentListPage;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/** Tests for {@link Reflection}. */
public class ReflectionTest extends TestBase
{

    private static final int EXPECTED_NUMBER_TYPE_ARGUMENTS_FOR_LIST = 1;
    private static FieldTypesComponent obj = new FieldTypesComponent();

    @Test
    public void retrieveFields()
    {
        Set<Field> fields = new HashSet<>( Arrays.asList(Reflection.getClassFields(obj)) );

        // we can see all expected fields, even though they are private
        assertTrue( fields.stream().anyMatch(field -> field.getName().equals("element")) );
        assertTrue( fields.stream().anyMatch(field -> field.getName().equals("listOfElements")) );
        assertTrue( fields.stream().anyMatch(field -> field.getName().equals("resolvable")) );
        assertTrue( fields.stream().anyMatch(field -> field.getName().equals("listOfResolvables")) );
    }

    @Test
    /* Retrieval and type checks for field of List<WebElement> type. */
    public void getTypeInfoForListOfWebElements()
    {
        Set<Field> fields = new HashSet<>( Arrays.asList(Reflection.getClassFields(obj)) );

        Optional<Field> listOfElementsField = fields.stream().filter(field -> field.getName().equals("listOfElements")).findFirst();
        assertTrue(listOfElementsField.isPresent());

        Type[] fieldTypeArguments = Reflection.getFieldTypeArguments(listOfElementsField.get());
        assertEquals( fieldTypeArguments.length, EXPECTED_NUMBER_TYPE_ARGUMENTS_FOR_LIST );
        assertEquals( fieldTypeArguments[0], WebElement.class,"type argument is not WebElement" );
    }

    @Test
    /* Retrieval and type checks for field of ResolvableList<Resolvable> type. */
    public void getTypeInfoForResolvableList()
    {
        WebDriverHandleFactory driverFactory = new TestDriverFactory();
        WebDriverHandle driverHandle = driverFactory.createDriverHandle(driverCriteria);
        TableRowComponentListPage page = new TableRowComponentListPage(driverHandle);

        Set<Field> pageFields = new HashSet<>( Arrays.asList(Reflection.getClassFields(page)) );
        Optional<Field> resolvableListFieldOpt = pageFields.stream().filter(field -> field.getName().equals("tableRows")).findFirst();
        assertTrue( resolvableListFieldOpt.isPresent() );

        Field resolvableListField = resolvableListFieldOpt.get();
        assertTrue(FieldOfResolvable.isList(resolvableListField) );
        assertTrue( FieldOfResolvable.isResolvableList(resolvableListField) );
        Type[] resolvableListFieldTypeArguments = Reflection.getFieldTypeArguments(resolvableListField);
        assertEquals( resolvableListFieldTypeArguments.length, EXPECTED_NUMBER_TYPE_ARGUMENTS_FOR_LIST );
        assertTrue(Resolvable.class.isAssignableFrom((Class) resolvableListFieldTypeArguments[0]), "type argument is not Resolvable" );

        driverHandle.getDriver().close();
    }
}