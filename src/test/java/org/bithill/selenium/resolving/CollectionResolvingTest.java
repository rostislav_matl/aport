package org.bithill.selenium.resolving;

import org.bithill.selenium.resolving.pageobjects.TableRowsNullableListPage;
import org.bithill.selenium.resolving.pageobjects.LinksCollectionInComponentLinkPage;
import org.bithill.selenium.resolving.pageobjects.LinksCollectionInlineLinkExtPage;
import org.bithill.selenium.resolving.pageobjects.LinksCollectionInlineLinkPage;
import org.bithill.selenium.resolving.pageobjects.TableRowComponentListPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/** Tests resolving of {@link WebElement} collections. */
public class CollectionResolvingTest extends TestBase
{
    public static final int EXPECTED_LINKS_COUNT = 7;

    /**
     * Resolves directly included collection of links as one of {@link WebElement}s, i.e.
     * {@link ResolveBy}-annotated List<WebElement> fields of the page class.
     */
    @Test
    public void resolveListOfWebElementsInPage()
    {
        LinksCollectionInlineLinkPage linkPage = new LinksCollectionInlineLinkPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        assertEquals(linkPage.linksByTag.size(), EXPECTED_LINKS_COUNT);
        assertEquals(linkPage.linksByXpath.size(), EXPECTED_LINKS_COUNT);
        for (int i = 0; i < EXPECTED_LINKS_COUNT; i++)
        {
            assertEquals(linkPage.linksByTag.get(0), linkPage.linksByXpath.get(0), "element #" + i + "does not match");
        }
    }

    /**
     * Resolves directly included collection of links as one of {@link WebElement}s, i.e.
     * {@link ResolveBy}-annotated List<WebElement> fields of the page class.
     *
     * Additionally checks that also inherited list fields are resolved.
     */
    @Test
    public void resolveListOfWebElementsInPageIncludingInherited()
    {
        LinksCollectionInlineLinkExtPage linkPage = new LinksCollectionInlineLinkExtPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        assertNotNull(linkPage.linksByTag2);

        // inherited field
        assertNotNull(linkPage.linksByTag);
        assertNotNull(linkPage.getLinksByTagPriv());
    }

    /**
     * Resolves collection of links in a component, i.e.
     * {@link ResolveBy}-annotated List<WebElement> fields of a class which is Resolvable but not a Page.
     */
    @Test
    public void resolveListOfWebElementsInComponent()
    {
        LinksCollectionInComponentLinkPage linkPage = new LinksCollectionInComponentLinkPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        Assert.assertEquals(EXPECTED_LINKS_COUNT, linkPage.links.linksByTag.size());
        Assert.assertEquals(EXPECTED_LINKS_COUNT, linkPage.links.linksByXpath.size());
        for (int i = 0; i < EXPECTED_LINKS_COUNT; i++)
        {
            Assert.assertEquals(linkPage.links.linksByTag.get(0), linkPage.links.linksByXpath
                    .get(0), "element #" + i + "does not match");
        }
    }

    /**
     * Resolves directly included collection of links as one of {@link Resolvable}s, i.e.
     * {@link ResolveBy}-annotated List<Resolvable> fields of the page class.
     */
    @Test
    public void resolveListOfResolvables()
    {
        TableRowComponentListPage tablePage = new TableRowComponentListPage(driverHandle);
        tablePage.open();
        tablePage.resolve();

        assertEquals(tablePage.tableRows.size(), 2);
        Assert.assertEquals(tablePage.tableRows.get(0).column1.getText(), "cell 1.1");
        Assert.assertEquals(tablePage.tableRows.get(0).column2.getText(), "cell 1.2");
        Assert.assertEquals(tablePage.tableRows.get(1).column1.getText(), "cell 2.1");
        Assert.assertEquals(tablePage.tableRows.get(1).column2.getText(), "cell 2.2");
    }

    /**
     * Resolves nullable collection of table rows as one of {@link Resolvable}s, i.e.
     * {@link ResolveBy}-annotated, Nullable-annotated List<Resolvable> field of the page class.
     */
    @Test
    public void resolveNullableListOfResolvables()
    {
        TableRowsNullableListPage tableRowsNullableListPage = new TableRowsNullableListPage(driverHandle);
        tableRowsNullableListPage.open();
        tableRowsNullableListPage.resolve();

        if (tableRowsNullableListPage.tableRows != null) {
            assertEquals(tableRowsNullableListPage.tableRows.size(), 2);
            assertEquals(tableRowsNullableListPage.tableRows.get(0).column1.getText(), "cell 1.1");
            assertEquals(tableRowsNullableListPage.tableRows.get(0).column2.getText(), "cell 1.2");
            assertEquals(tableRowsNullableListPage.tableRows.get(1).column1.getText(), "cell 2.1");
            assertEquals(tableRowsNullableListPage.tableRows.get(1).column2.getText(), "cell 2.2");
        }
    }
}
