package org.bithill.selenium.resolving;

import java.util.HashMap;
import java.util.Map;

import org.bithill.selenium.TestDriverFactory;
import org.bithill.selenium.driver.WebDriverHandle;
import org.bithill.selenium.driver.WebDriverHandleFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public abstract class TestBase
{
    private WebDriverHandleFactory driverFactory = new TestDriverFactory();
    WebDriverHandle driverHandle;

    static final Map<String, String> driverCriteria = new HashMap<>(1);
    static
    {
        driverCriteria.put("driver", "HtmlUnitDriver");
    }

    @BeforeClass
    public void setup()
    {
        driverHandle = driverFactory.createDriverHandle(driverCriteria);
    }

    @AfterClass
    public void teardown()
    {
        driverHandle.getDriver().close();
    }
}
