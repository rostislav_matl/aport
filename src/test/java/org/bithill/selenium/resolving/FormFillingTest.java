package org.bithill.selenium.resolving;

import java.util.Properties;

import org.bithill.selenium.resolving.pageobjects.FormPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/** Tests resolving and filling of a form. */
public class FormFillingTest extends TestBase
{
    private static final String TEXT_VALUE = "123";
    private static final String PASSWORD_VALUE = "*123*";
    private static final String SELECT_LABEL = "value y";

    @Test
    public void fillForm()
    {
        FormPage page = new FormPage(driverHandle);
        page.open();
        page.resolve();

        Properties formData = new Properties();
        formData.setProperty("text",TEXT_VALUE);
        formData.setProperty("password",PASSWORD_VALUE);
        formData.setProperty("select", SELECT_LABEL);

        page.loadData(formData);

        assertEquals(page.getText().getAttribute("value"), TEXT_VALUE);
        assertEquals(page.getPassword().getAttribute("value"), PASSWORD_VALUE);
        assertEquals(page.getSelect().getFirstSelectedOption().getText(), SELECT_LABEL);
    }
}
