package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.Page;
import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;

/**
 * Empty page with a single locator that point to non-existing WebElement.
 * The element is put in a separate component.
 */
@Site(config = ResourceFileConfig.class)
public class NonExistentLinkComponentPage extends Page
{
    public NonExistentLinkComponentPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
        setPageUrlParams("empty.html");
    }

    public NonExistentLinkComponent links;

    @Override
    protected NonExistentLinkComponentPage checkOpened()
    {
        return this;
    }

    @Override
    public Page open()
    {
        return super.open();
    }
}