package org.bithill.selenium.resolving.pageobjects;

import java.util.List;

import org.bithill.selenium.resolving.Resolvable;
import org.openqa.selenium.WebElement;

/**
 * Resolvable used for testing of reflection methods.
 */
public class FieldTypesComponent extends Resolvable
{

    private WebElement element;
    private List<WebElement> listOfElements;

    private FieldTypesComponent resolvable;
    private List<FieldTypesComponent> listOfResolvables;
}
