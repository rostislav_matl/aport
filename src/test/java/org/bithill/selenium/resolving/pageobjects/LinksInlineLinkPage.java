package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;
import org.openqa.selenium.WebElement;

import org.bithill.selenium.resolving.pageobjects.LinkBasePage;

/** Page class for links.html, {@link WebElement}s representing links are inserted into the page directly . */
@Site(config = ResourceFileConfig.class)
public class LinksInlineLinkPage extends LinkBasePage
{
    @ResolveBy("id=target_link_id")
    public WebElement linkById;

    @ResolveBy("id=target_link_id")
    private WebElement linkByIdPriv;
    public WebElement getLinkByIdPriv() { return linkByIdPriv; }

    @ResolveBy("name=target_link_name")
    public WebElement linkByName;

    @ResolveBy("xpath=//a[3]")
    public WebElement linkByXpath;

    @ResolveBy("tag=a")
    public WebElement linkByTag;

    @ResolveBy("link=link with nothing special")
    public WebElement linkByLinkText;

    @ResolveBy("css=a.my-link")
    public WebElement linkByCss;

    public LinksInlineLinkPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
    }
}
