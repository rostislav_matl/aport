package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.Resolvable;
import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.driver.WebDriverHandle;
import org.openqa.selenium.WebElement;

/** Component representing a row of table with two columns. Columns are resolved relatively to a row. */
public class TableRowComponent extends Resolvable
{
    @ResolveBy("xpath=./td[1]")
    public WebElement column1;

    @ResolveBy("xpath=./td[2]")
    public WebElement column2;

    public TableRowComponent(WebDriverHandle driverHandle)
    {
        super(driverHandle);
    }
}
