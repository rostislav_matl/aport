package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;
import org.openqa.selenium.WebElement;

/** Page class for links.html, {@link WebElement}s representing links are inserted into the page directly.
 *  Used for inheritance tests. */
@Site(config = ResourceFileConfig.class)
public class LinksInlineLinkExtPage extends LinksInlineLinkPage
{
   @ResolveBy("id=target_link_id")
    public WebElement linkById2;

    public LinksInlineLinkExtPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
    }
}
