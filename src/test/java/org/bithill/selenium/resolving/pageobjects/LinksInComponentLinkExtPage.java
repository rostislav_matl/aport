package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;

/** Page class for links.html, links represented by an included component.
 *  Used for inheritance tests. */
@Site(config = ResourceFileConfig.class)
public class LinksInComponentLinkExtPage extends LinksInComponentLinkPage
{
    public LinksComponent links2;

    public LinksInComponentLinkExtPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
    }
}
