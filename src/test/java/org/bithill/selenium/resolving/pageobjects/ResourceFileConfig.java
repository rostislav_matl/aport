package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.site.SiteConfig;

/** Configuration for pages from resource directory. */
public class ResourceFileConfig extends SiteConfig
{
    @Override
    public String getUrl()
    {
        return "file://" + System.getProperty("user.dir") + "/src/test/resources/html/";
    }
}
