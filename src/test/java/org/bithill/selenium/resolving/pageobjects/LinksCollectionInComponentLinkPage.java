package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;

/** Page class for links.html, collections of links represented by an included component. */
@Site(config = ResourceFileConfig.class)
public class LinksCollectionInComponentLinkPage extends LinkBasePage
{
    public LinksCollectionComponent links;

    public LinksCollectionInComponentLinkPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
    }
}
