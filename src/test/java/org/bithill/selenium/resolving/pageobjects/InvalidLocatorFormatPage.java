package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.Page;
import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;
import org.openqa.selenium.WebElement;

/** Empty page with a single invalid locator. */
@Site(config = ResourceFileConfig.class)
public class InvalidLocatorFormatPage extends Page
{
    public InvalidLocatorFormatPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
        setPageUrlParams("empty.html");
    }

    @ResolveBy("invalid-locator-no-delimiter")
    public WebElement element;

    @Override
    protected InvalidLocatorFormatPage checkOpened()
    {
        return this;
    }
}