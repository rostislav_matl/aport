package org.bithill.selenium.resolving.pageobjects;

import java.util.List;

import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;
import org.openqa.selenium.WebElement;

/** Page class for links.html, {@link WebElement}s representing links are inserted into the page directly .
 *  Used for inheritance tests. */
@Site(config = ResourceFileConfig.class)
public class LinksCollectionInlineLinkExtPage extends LinksCollectionInlineLinkPage
{
    @ResolveBy("tag=a")
    public List<WebElement> linksByTag2;

    public LinksCollectionInlineLinkExtPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
    }
}
