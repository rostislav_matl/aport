package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;

/** Page class for links.html, links represented by an included component. */
@Site(config = ResourceFileConfig.class)
public class LinksInComponentWithSelfLinkPage extends LinkBasePage
{
    @ResolveBy(value = "id=links")
    public LinksComponent links;

    public LinksInComponentWithSelfLinkPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
    }
}
