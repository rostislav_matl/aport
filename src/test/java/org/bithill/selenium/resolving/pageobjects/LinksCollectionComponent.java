package org.bithill.selenium.resolving.pageobjects;

import java.util.List;

import org.bithill.selenium.resolving.Resolvable;
import org.bithill.selenium.resolving.ResolveBy;
import org.openqa.selenium.WebElement;

/** Component representing collections od links in a page. */
public class LinksCollectionComponent extends Resolvable
{
    @ResolveBy("tag=a")
    public List<WebElement> linksByTag;

    @ResolveBy("xpath=//a")
    public List<WebElement> linksByXpath;
}
