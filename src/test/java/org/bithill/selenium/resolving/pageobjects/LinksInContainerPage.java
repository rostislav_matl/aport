package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;

/** Page class for links.html, links represented by an included component. */
@Site(config = ResourceFileConfig.class)
public class LinksInContainerPage extends LinkBasePage
{
    @ResolveBy(value = "id=links2", isContainer = true)
    public LinksComponentSmall links;

    public LinksInContainerPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
    }
}
