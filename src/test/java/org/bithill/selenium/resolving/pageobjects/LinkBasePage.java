package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.InvalidPageException;
import org.bithill.selenium.resolving.Page;
import org.bithill.selenium.driver.WebDriverHandle;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/** Shared functionality for all the link pages. */
abstract class LinkBasePage extends Page
{
    LinkBasePage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
        setPageUrlParams("links.html");
    }

    @Override
    protected Page checkOpened()
    {
        WebElement title = getDriver().findElement(By.id("title"));
        if (!"links".equals(title.getText()))
        {
            throw new InvalidPageException("wrong page opened");
        }
        return this;
    }
}
