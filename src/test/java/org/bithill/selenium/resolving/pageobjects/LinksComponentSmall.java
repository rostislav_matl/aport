package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.Resolvable;
import org.bithill.selenium.resolving.ResolveBy;
import org.openqa.selenium.WebElement;

/** Component representing portion of a page with several links. Reduced. */
public class LinksComponentSmall extends Resolvable
{
    @ResolveBy("name=target_link_name")
    public WebElement linkByName;

    @ResolveBy("css=a.my-link")
    public WebElement linkByCss;
}
