package org.bithill.selenium.resolving.pageobjects;

import java.util.List;

import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;
import org.openqa.selenium.WebElement;

/** Page class for links.html, {@link WebElement}s representing links are inserted into the page directly . */
@Site(config = ResourceFileConfig.class)
public class LinksCollectionInlineLinkPage extends LinkBasePage
{
    @ResolveBy("tag=a")
    public List<WebElement> linksByTag;

    @ResolveBy("tag=a")
    private List<WebElement> linksByTagPriv;
    public List<WebElement> getLinksByTagPriv() { return linksByTagPriv; }

    @ResolveBy("xpath=//a")
    public List<WebElement> linksByXpath;

    public LinksCollectionInlineLinkPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
    }
}
