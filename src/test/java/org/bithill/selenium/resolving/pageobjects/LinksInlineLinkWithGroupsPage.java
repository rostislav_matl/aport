package org.bithill.selenium.resolving.pageobjects;

import java.util.List;

import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;
import org.openqa.selenium.WebElement;

/** Page class for links.html, {@link WebElement}s representing links are inserted into the page directly .
 *  This variant uses groups to separate sets of elements.
 */
@Site(config = ResourceFileConfig.class)
public class LinksInlineLinkWithGroupsPage extends LinkBasePage
{

    @ResolveBy(value = "id=target_link_id", groups = {"group-one"} )
    public WebElement linkById;

    @ResolveBy(value = "name=target_link_name", groups = {"default", "group-one"})
    public WebElement linkByName;

    @ResolveBy(value = "xpath=//a[3]")
    public WebElement linkByXpath;

    @ResolveBy(value = "tag=a", groups = {"group-one"})
    public WebElement linkByTag;

    @ResolveBy(value = "link=link with nothing special")
    public WebElement linkByLinkText;

    @ResolveBy(value = "css=a.my-link")
    public WebElement linkByCss;

    @ResolveBy(value = "xpath=//a", groups = {"group-one"})
    public List<WebElement> links;

    public LinksInlineLinkWithGroupsPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
    }
}
