package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.Page;
import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;

/**
 * Empty page with a list locators that point to no existing WebElement.
 * The list of elements is put in a separate component.
 */
@Site(config = ResourceFileConfig.class)
public class NonExistentLinkListComponentPage extends Page
{
    public NonExistentLinkListComponentPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
        setPageUrlParams("empty.html");
    }

    public NonExistentLinkListComponent links;

    @Override
    protected NonExistentLinkListComponentPage checkOpened()
    {
        return this;
    }

    @Override
    public Page open()
    {
        return super.open();
    }
}