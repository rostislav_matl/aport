package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.Page;
import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;
import org.openqa.selenium.WebElement;

/**
 * Empty page with a single locator that point to non-existing WebElement.
 * The element is inserted directly into the page.
 */
@Site(config = ResourceFileConfig.class)
public class NonExistentLocatorPage extends Page
{
    public NonExistentLocatorPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
        setPageUrlParams("empty.html");
    }

    @ResolveBy("id=mystery-id")
    public WebElement nonExistentElement;

    @Override
    protected NonExistentLocatorPage checkOpened()
    {
        return this;
    }

    @Override
    public Page open()
    {
        return super.open();
    }
}