package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.resolving.Resolvable;
import org.bithill.selenium.resolving.ResolveBy;
import org.openqa.selenium.WebElement;

/** Component representing portion of a page with several links. */
public class LinksComponent extends Resolvable
{
    @ResolveBy("id=target_link_id")
    public WebElement linkById;

    @ResolveBy("name=target_link_name")
    public WebElement linkByName;

    @ResolveBy("xpath=//a[3]")
    public WebElement linkByXpath;

    @ResolveBy("tag=a")
    public WebElement linkByTag;

    @ResolveBy("link=link with nothing special")
    public WebElement linkByLinkText;

    @ResolveBy("css=a.my-link")
    public WebElement linkByCss;
}
