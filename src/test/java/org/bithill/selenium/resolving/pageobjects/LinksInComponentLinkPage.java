package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.site.Site;
import org.bithill.selenium.driver.WebDriverHandle;

/** Page class for links.html, links represented by an included component. */
@Site(config = ResourceFileConfig.class)
public class LinksInComponentLinkPage extends LinkBasePage
{
    public LinksComponent links;

    private LinksComponent linksPriv;
    public LinksComponent getLinksPriv() { return linksPriv; }

    public LinksInComponentLinkPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
    }
}
