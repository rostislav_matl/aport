package org.bithill.selenium.resolving.pageobjects;

import javax.annotation.Nullable;

import org.bithill.selenium.driver.WebDriverHandle;
import org.bithill.selenium.resolving.InvalidPageException;
import org.bithill.selenium.resolving.Page;
import org.bithill.selenium.resolving.ResolvableList;
import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.site.Site;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/** Page class for table.html, nullable collection of rows represented by a list of resolvables. */
@Site(config = ResourceFileConfig.class)
public class TableRowsNullableListPage extends Page
{
    @Nullable
    @ResolveBy(value = "xpath=//table//tr", isContainer = true)
    public ResolvableList<TableRowComponent> tableRows;

    public TableRowsNullableListPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
        setPageUrlParams("table.html");
    }

    @Override
    protected Page checkOpened()
    {
        WebElement title = getDriver().findElement(By.id("title"));
        if (!"page with a table".equals(title.getText()))
        {
            throw new InvalidPageException("wrong page opened");
        }
        return this;
    }
}
