package org.bithill.selenium.resolving.pageobjects;

import javax.annotation.Nullable;

import org.bithill.selenium.resolving.Resolvable;
import org.bithill.selenium.resolving.ResolveBy;
import org.openqa.selenium.WebElement;

/**
 * Component for a portion of a page.
 * Contains a single {@link WebElement} which is not on the page, i.e. its resolving should fail.
 */
public class NonExistentLinkComponent extends Resolvable
{
    @ResolveBy("id=nullable")
    @Nullable
    public WebElement nullableLink;

    @ResolveBy("id=nonnullable")
    public WebElement nonNullableLink;
}
