package org.bithill.selenium.resolving.pageobjects;

import org.bithill.selenium.driver.WebDriverHandle;
import org.bithill.selenium.resolving.InvalidPageException;
import org.bithill.selenium.resolving.Page;
import org.bithill.selenium.resolving.ResolveBy;
import org.bithill.selenium.site.Site;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/** Shared functionality for all the link pages. */
@Site(config = ResourceFileConfig.class)
public class FormPage extends Page
{

    @ResolveBy("id=textinput")
    private WebElement text;
    public WebElement getText() { return text; }

    @ResolveBy("id=passwordinput")
    private WebElement password;
    public WebElement getPassword() { return password; }

    @ResolveBy("id=selectinput")
    private Select select;
    public Select getSelect() { return select; }

    public FormPage(WebDriverHandle driverHandle)
    {
        super(driverHandle);
        setPageUrlParams("form.html");
    }

    @Override
    protected Page checkOpened()
    {
        WebElement title = getDriver().findElement(By.id("title"));
        if (!"form #1".equals(title.getText()))
        {
            throw new InvalidPageException("wrong page opened");
        }
        return this;
    }
}
