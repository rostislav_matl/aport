package org.bithill.selenium.resolving.pageobjects;

import javax.annotation.Nullable;
import java.util.List;

import org.bithill.selenium.resolving.Resolvable;
import org.bithill.selenium.resolving.ResolveBy;
import org.openqa.selenium.WebElement;

/**
 * Component for a portion of a page.
 * Contains lists of {@link WebElement}s which are not on the page, i.e. its resolving should fail.
 */
public class NonExistentLinkListComponent extends Resolvable
{
    @ResolveBy("xpath=//nullable")
    @Nullable
    public List<WebElement> nullableLinks;

    @ResolveBy("xpath=//nonnullable")
    public List<WebElement> nonNullableLinks;
}
