package org.bithill.selenium.resolving;

import java.util.List;

import org.bithill.selenium.resolving.pageobjects.LinksInlineLinkWithGroupsPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.bithill.selenium.resolving.ResolveBy.DEFAULT_GROUP;

/** Tests resolving of inline {@Link WebElement}s using different locators AND groups. */
public class GroupResolveAndFindTest extends TestBase
{
    final String[] GROUPS = {"group-one"};

    /**
     * Resolves links belonging to the default group and list them using Find.
     */
    @Test
    public void resolveDirectlyIncludedLinksInDefaultGroup()
    {
        LinksInlineLinkWithGroupsPage linkPage = new LinksInlineLinkWithGroupsPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        List<WebElement> groupMembers = Find.in(linkPage).groupMembers(DEFAULT_GROUP);

        Assert.assertNotNull(groupMembers);
        // expected size is 3 because linkByXpath and linkByText are the same
        Assert.assertEquals(groupMembers.size(),3);
    }

    /**
     * Resolves links belonging to the GROUP group and list them using Find.
     */
    @Test
    public void resolveDirectlyIncludedLinksInNamedGroup()
    {
        LinksInlineLinkWithGroupsPage linkPage = new LinksInlineLinkWithGroupsPage(driverHandle);
        linkPage.open();
        linkPage.resolve(GROUPS);

        List<WebElement> groupMembers = Find.in(linkPage).groupMembers(GROUPS);

        Assert.assertNotNull(groupMembers);
        // expected size is 5 the collection of all links is in the group and there is 5 link on the page
        Assert.assertEquals(groupMembers.size(),7);
    }
}
