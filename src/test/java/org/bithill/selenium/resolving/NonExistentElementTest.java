package org.bithill.selenium.resolving;

import org.bithill.selenium.driver.WebDriverHandle;
import org.bithill.selenium.resolving.pageobjects.NonExistentLinkComponent;
import org.bithill.selenium.resolving.pageobjects.NonExistentLinkComponentPage;
import org.bithill.selenium.resolving.pageobjects.NonExistentLinkListComponent;
import org.bithill.selenium.resolving.pageobjects.NonExistentLinkListComponentPage;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.remote.RemoteWebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/** Test of cases when the locator has proper syntax but does not point to an element in a page. */
public class NonExistentElementTest extends TestBase
{

    final String NON_RESOLVABLE_NON_NULLABLE = "of element located by By.id: nonnullable";
    final String NON_RESOLVABLE_NON_NULLABLE_LIST = "all elements located by By.xpath: //nonnullable";

    final String TIMEOUT_MESSAGE_TEMPLATE = "(tried for %d second(s) with 500 milliseconds interval)";

    @Test
    public void timeoutExceptionThrownForNonExistentNonNullableElement()
    {
        Page page = new NonExistentLinkComponentPage(driverHandle);
        boolean expectedExceptionThrown = false;
        try
        {
            page.resolve();
        }
        catch (TimeoutException ex)
        {
            expectedExceptionThrown = true;
            String timeoutMessage = String.format(TIMEOUT_MESSAGE_TEMPLATE, WebDriverHandle.DEFAULT_TIMEOUT_SECONDS );
            assertTrue(ex.getMessage().contains(NON_RESOLVABLE_NON_NULLABLE + " " + timeoutMessage));
        }
        assertTrue(expectedExceptionThrown,"org.openqa.selenium.TimeoutException should have been thrown");
    }

    @Test
    public void nonExistentNullableElementResolvedToNull()
    {
        NonExistentLinkComponentPage page = new NonExistentLinkComponentPage(driverHandle);

        // setting non-existent nullable element to some not-null value
        page.links = new NonExistentLinkComponent();
        page.links.nullableLink = new RemoteWebElement();

        try
        {
            assertNotNull(page.links.nullableLink);
            // resolve should set it to null
            page.resolve();
        }
        catch (TimeoutException ex)
        {
            /** ignored, exception for the second, non-nullable field */
        }
        finally
        {
            assertNull(page.links.nullableLink);
        }
    }


    @Test
    public void timeoutExceptionThrownForNonExistentNonNullableListOfElements()
    {
        Page page = new NonExistentLinkListComponentPage(driverHandle);
        boolean expectedExceptionThrown = false;
        try
        {
            page.resolve();
        }
        catch (TimeoutException ex)
        {
            expectedExceptionThrown = true;
            String timeoutMessage = String.format(TIMEOUT_MESSAGE_TEMPLATE, WebDriverHandle.DEFAULT_TIMEOUT_SECONDS );
            assertTrue(ex.getMessage().contains(NON_RESOLVABLE_NON_NULLABLE_LIST + " " + timeoutMessage));
        }
        assertTrue(expectedExceptionThrown,"org.openqa.selenium.TimeoutException should have been thrown");
    }

    @Test
    public void nonExistentNullableListOfElementsResolvedToEmptyList()
    {
        NonExistentLinkListComponentPage page = new NonExistentLinkListComponentPage(driverHandle);

        // setting non-existent nullable list of element to some non-empty list
        page.links = new NonExistentLinkListComponent();

        try
        {
            assertNull(page.links.nullableLinks);
            // resolve should set it to an empty list
            page.resolve();
        }
        catch (TimeoutException ex)
        {
            /** ignored, exception for the second, non-nullable field */
        }
        finally
        {
            assertTrue(page.links.nullableLinks.isEmpty());
        }
    }
}
