package org.bithill.selenium.resolving;

import org.bithill.selenium.resolving.pageobjects.LinksInlineLinkWithGroupsPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/** Tests resolving of inline {@Link WebElement}s using different locators AND groups. */
public class GroupResolvingTest extends TestBase
{
    final String GROUP = "group-one";

    /**
     * Resolves links belonging to the default group - elements not belonging to the group must be null.
     */
    @Test
    public void resolveDirectlyIncludedLinksInDefaultGroup()
    {
        LinksInlineLinkWithGroupsPage linkPage = new LinksInlineLinkWithGroupsPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        // should be null as they belong only to GROUP
        assertNull(linkPage.linkById, "link with id is not in the default group, should be NULL");
        assertNull(linkPage.linkByTag, "link located by tag is not in the default group, should be NULL");

        // should be resolved as it belong both to 'default' and GROUP
        assertEquals(linkPage.linkByName.getText(), "link with name");

        // should be resolved as they belong only to the group 'default'
        assertEquals(linkPage.linkByXpath.getText(), "link with nothing special");
        assertEquals(linkPage.linkByLinkText.getText(), "link with nothing special");
        assertEquals(linkPage.linkByCss.getText(), "link with CSS class");
    }

    /**
     * Resolves links belonging to a specific group - elements not belonging to the group must be null.
     */
    @Test
    public void resolveDirectlyIncludedLinksInNamedGroup()
    {
        LinksInlineLinkWithGroupsPage linkPage = new LinksInlineLinkWithGroupsPage(driverHandle);
        linkPage.open();
        linkPage.resolve(GROUP);

        // should be resolved as they belong only to GROUP
        assertEquals(linkPage.linkById.getText(), "link with id");
        assertEquals(linkPage.linkByTag.getText(), "link with id");

        // should be resolved as it belong both to 'default' and GROUP
        assertEquals(linkPage.linkByName.getText(), "link with name");

        // should be null as they belong only to the group 'default'
        assertNull(linkPage.linkByXpath, "link located by xpath should be null, it does not belong to group " + GROUP);
        assertNull(linkPage.linkByLinkText, "link located by text should be null, it does not belong to group " + GROUP);
        assertNull(linkPage.linkByCss, "link with CSS class should be null, it does not belong to group " + GROUP);
    }

    /**
     * Resolves links belonging to a specific group and group 'default', i.e. two groups at once.
     * All elements should be resolved as they belong to one of the groups or both at once.
     */
    @Test
    public void resolveDirectlyIncludedLinksInTwoGroups()
    {
        LinksInlineLinkWithGroupsPage linkPage = new LinksInlineLinkWithGroupsPage(driverHandle);
        linkPage.open();
        linkPage.resolve("default",GROUP);

        // should be resolved as they belong only to GROUP
        assertEquals(linkPage.linkById.getText(), "link with id");
        assertEquals(linkPage.linkByTag.getText(), "link with id");

        // should be resolved as it belong both to 'default' and GROUP
        assertEquals(linkPage.linkByName.getText(), "link with name");

        // should be resolved as they belong only to the group 'default'
        assertEquals(linkPage.linkByXpath.getText(), "link with nothing special");
        assertEquals(linkPage.linkByLinkText.getText(), "link with nothing special");
        assertEquals(linkPage.linkByCss.getText(), "link with CSS class");
    }
}
