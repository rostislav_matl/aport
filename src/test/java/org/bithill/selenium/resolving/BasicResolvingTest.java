package org.bithill.selenium.resolving;

import org.testng.Assert;
import org.bithill.selenium.resolving.pageobjects.InvalidLocatorFormatPage;
import org.bithill.selenium.resolving.pageobjects.LinksInlineLinkExtPage;
import org.bithill.selenium.resolving.pageobjects.LinksInlineLinkPage;
import org.bithill.selenium.resolving.pageobjects.NonExistentLocatorPage;
import org.bithill.selenium.resolving.pageobjects.UnknownLocatorTypePage;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/** Tests resolving of inline {@link WebElement}s using different locators. */
public class BasicResolvingTest extends TestBase
{
    /**
     * Resolves links as one of {@link WebElement}s with the most wide palette of locators.
     * In this case we check only WebElements directly included in the page, i.e.
     * {@link ResolveBy}-annotated WebElement fields of the page class.
     */
    @Test
    public void resolveDirectlyIncludedLinks()
    {
        LinksInlineLinkPage linkPage = new LinksInlineLinkPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        org.testng.Assert.assertEquals(linkPage.linkById.getText(), "link with id");
        org.testng.Assert.assertEquals(linkPage.linkByName.getText(), "link with name");

        org.testng.Assert.assertEquals(linkPage.linkByXpath.getText(), "link with nothing special");
        org.testng.Assert.assertEquals(linkPage.linkByLinkText.getText(), "link with nothing special");

        // link with id is the first one, therefore it should be resolved by the tag locator
        org.testng.Assert.assertEquals(linkPage.linkByTag.getText(), "link with id");

        org.testng.Assert.assertEquals(linkPage.linkByCss.getText(), "link with CSS class");
    }

    @Test
    public void resolveDirectlyIncludedLinksInherited()
    {
        LinksInlineLinkExtPage linkPage = new LinksInlineLinkExtPage(driverHandle);
        linkPage.open();
        linkPage.resolve();

        assertNotNull(linkPage.linkById2);

        // inherited field
        assertNotNull(linkPage.linkById);
        assertNotNull(linkPage.getLinkByIdPriv());
    }

    @Test
    /** Checks that locator not matching a WebElement in the page throws appropriate Selenium exception. */
    public void failToResolveNonExistingLocator()
    {
        Page page = new NonExistentLocatorPage(driverHandle);
        page.open();

        try
        {
            page.resolve();
        }
        catch (TimeoutException ex)
        {
            // check if the info about the unresolved field is available
            String fieldInException = "waiting for visibility of element located by By.id: mystery-id";
            assertTrue(ex.getMessage().contains(fieldInException), "Information about the relevant identifier is missing.");

            // check if the cause matches the expectation
            Throwable cause = ex.getCause();
            assertEquals(cause.getClass().getName(), "org.openqa.selenium.NoSuchElementException",
                         "Timeout should have been caused by NoSuchElementException.");

            // check the error message
            assertTrue(cause.getMessage().contains("Cannot locate an element using By.id: mystery-id"));
        }
        catch (Exception ex)
        {
            Assert.fail("Unexpected exception: " + ex);
        }
    }

    @Test(expectedExceptions = {UnknownLocatorTypeException.class},
          expectedExceptionsMessageRegExp = "Unknown locator type: 'invalid-locator'.")
    /** Checks that use of a locator with unknown type throws UnknownLocatorTypeException. */
    public void failToResolveLocatorWithUnknownType()
    {
        UnknownLocatorTypePage page = new UnknownLocatorTypePage(driverHandle);
        page.open();
        page.resolve();
    }

    @Test(expectedExceptions = {InvalidLocatorFormatException.class},
          expectedExceptionsMessageRegExp = "Locator has invalid format: 'invalid-locator-no-delimiter'.")
    /** Checks that use of a locator with invalid format throws InvalidLocatorFormatException. */
    public void failToResolveLocatorWithInvalidFormat()
    {
        InvalidLocatorFormatPage page = new InvalidLocatorFormatPage(driverHandle);
        page.open();
        page.resolve();
    }
}
