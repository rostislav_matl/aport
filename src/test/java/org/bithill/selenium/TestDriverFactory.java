package org.bithill.selenium;

import java.util.Map;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.bithill.selenium.driver.WebDriverHandle;
import org.bithill.selenium.driver.WebDriverHandleFactory;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class TestDriverFactory implements WebDriverHandleFactory<WebDriverHandle>
{
    @Override
    public WebDriverHandle createDriverHandle(Map<String,String> criteria)
    {
        WebDriverHandle driverHandle;

        String driverType = criteria.get("driver");
        switch (driverType)
        {
            case "HtmlUnitDriver":
                driverHandle = new WebDriverHandle(new HtmlUnitDriver(),false);
                break;

            case "ChromeDriver":
                DesiredCapabilities capabilities = setupChromeDriver();
                driverHandle = new WebDriverHandle(new ChromeDriver(capabilities),false);
                break;

            default:
                throw new IllegalArgumentException("Unknown driver type '" + driverType + "'");
        }

        return driverHandle;
    }

    private DesiredCapabilities setupChromeDriver() {

        ChromeDriverManager.getInstance().setup();

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--lang=en","--disable-extensions","--dns-prefetch-disable","--disable-bundled-ppapi-flash","--disable-infobars");

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);

        return capabilities;
    }
}
